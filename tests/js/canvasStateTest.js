oroboros(document).ready(function (e) {
    oroboros().info();
    console.info("This is a state pattern test.");
    window.stateTest = new oroboros.app.pattern.State({
        initialize: function () {
            console.info("This is the initialization step.");
            window.stateTest.next();
        },
        update: function () {
            console.info("This is the update step.");
            window.stateTest.next();
        },
        draw: function () {
            console.info("This is the redraw step.");
            
        },
    });

    window.coin = new window.oroboros.app.lib.Sprite("coinSpin", {
        width: 100,
        height: 100,
        image: "img/coin-sprite-animation.png",
        numberOfFrames: 8,
        ticksPerFrame: 6
    }).attach("#stage");
    window.runner = new window.oroboros.app.lib.Sprite("runner", {
        width: 100,
        height: 100,
        image: "img/running_test.png",
        numberOfFrames: 5,
        ticksPerFrame: 5
    }).attach("#stage");
    window.pose = new window.oroboros.app.lib.Sprite("posetest", {
        width: 362,
        height: 330,
        image: "img/pose_test_1.png",
        numberOfFrames: 6,
        ticksPerFrame: 5
    }).attach("#stage");
    coin.play();
    runner.play();
});