/*
 * ---------------------------- Plugin Boilerplate -----------------------------
 * This script demonstrates how to properly instantiate an OroborosJS plugin.
 * Feel free to modify this as needed to develop your own plugins.
 * -----------------------------------------------------------------------------
 */

oroboros().plugin("test", function (root) {
    var SIGNATURE = "Test Plugin",
        VERSION = "0.0.1",
        AUTHOR = "Brian Dayhoff",
        LICENSE = "MIT License";

    TestPlugin = function (selector, context, root) {
        //do stuff here
    };

    TestPlugin.prototype.app = root.app;
    TestPlugin.prototype.constructor = TestPlugin;
    TestPlugin.prototype.SIGNATURE = SIGNATURE;
    TestPlugin.prototype.VERSION = VERSION;
    TestPlugin.prototype.AUTHOR = AUTHOR;
    TestPlugin.prototype.LICENSE = LICENSE;

    return TestPlugin;
});