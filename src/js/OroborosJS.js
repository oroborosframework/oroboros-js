/*!
 * Oroboros Javascript Library v: 0.0.1
 * Copyright Brian Dayhoff
 * Released under the MIT License
 */

(function (global, constructor) {
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = global.document ?
                constructor(global, true) :
                function (w) {
                    if (!w.document) {
                        throw new Error("OroborosJS requires a window with a document");
                    }
                    return constructor(w);
                };
    } else {
        constructor(global);
    }
})(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
    var version = "0.0.1";
    var classToType = [];
    var deletedIds = [];
    var document = window.document;
    var slice = deletedIds.slice;
    var concat = deletedIds.concat;
    var push = deletedIds.push;
    var indexOf = deletedIds.indexOf;
    var class2type = {};
    var toString = class2type.toString;
    var hasOwn = class2type.hasOwnProperty;
    var support = {};
    var _OroborosRoot;
    var _options = {
        foo: "bar"
    };
    var init;
    var rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    var OroborosJS = function (selector, context) {
        return new OroborosJS.app.init(selector, context);
    };

    /**
     * Define the root object prototype
     */
    OroborosJS.app = OroborosJS.prototype = {
        oroboros: version,
        constructor: OroborosJS,
        selector: "",
        length: 0,
        options: function (command, key, value) {
            if (typeof OroborosJS.app.utility.options[command] !== "undefined") {
                return OroborosJS.app.utility.options[command](key, value);
            }
            return false;
        },
        info: function () {
            console.groupCollapsed("OroborosJS v: 0.0.1");
            console.log('Copyright Brian Dayhoff, all rights reserved.');
            console.log('Released under the MIT License');
            console.groupEnd();
        },
        error: function (message, level, code) {
            return new OroborosJS.app.lib.Error(message, level, code);
        },
        type: function () {
            return this.utility.type(this.selector);
        },
        each: function (obj, callback) {
            var length, i = 0;
            if (OroborosJS.app.utility.array.like(obj)) {
                length = obj.length;
                for (; i < length; i++) {
                    if (callback.call(obj[ i ], i, obj[ i ]) === false) {
                        break;
                    }
                }
            } else {
                for (i in obj) {
                    if (callback.call(obj[ i ], i, obj[ i ]) === false) {
                        break;
                    }
                }
            }
            return obj;
        },
        lib: {
            Application: function (index, args, params) {
                var i, prop, properties = {
                    loop: new OroborosJS.app.lib.Loop(),
                    stage: undefined,
                    state: new OroborosJS.app.pattern.State({}),
                    data: new OroborosJS.app.pattern.Iterator({}),
                    log: new OroborosJS.app.lib.Log(),
                }, stage;
                var OroborosApplication = function (index, args, params) {
                    this.applicationId = index;
                };
                OroborosApplication.prototype = window.oroboros.app;
                OroborosApplication.prototype.constructor = OroborosApplication;

                return new OroborosApplication(index, args, params);
            },
            Loop: function (index, args, params) {
                var properties = {},
                        data = new OroborosJS.app.pattern.Iterator((params || {})),
                        events = new OroborosJS.app.pattern.Iterator((params || {})),
                        log = new OroborosJS.app.lib.Log(),
                        loop = new OroborosJS.app.lib.Timer(),
                        state = new OroborosJS.app.pattern.State(),
                        OroborosLoop = function (index, args, params) {
                            return (this instanceof Loop ? this : new Loop(index, args, params));
                        };
                OroborosLoop.prototype.app = Loop.prototype = window.oroboros.app;
                OroborosLoop.prototype.constructor = Loop;

                return new OroborosLoop(index, args, params);
            },
            Closure: function (selector, context, root) {
            },
            Data: function (selector, context, root) {
            },
            Error: function (message, level, code) {
                //use strict
                this.app = this.prototype = OroborosJS.app;
                var _levels = {
                    emergency: {
                        debug: console.error,
                        throw: true,
                    },
                    alert: {
                        debug: console.error,
                        throw: true,
                    },
                    critical: {
                        debug: console.error,
                        throw: true,
                    },
                    error: {
                        debug: console.error,
                        throw: true,
                    },
                    warning: {
                        debug: console.warn,
                        throw: true,
                    },
                    notice: {
                        debug: console.info,
                        throw: false,
                    },
                    info: {
                        debug: console.info,
                        throw: false,
                    },
                    debug: {
                        debug: console.debug,
                        throw: false,
                    },
                };
                if (typeof message === "object" && message instanceof Error) {
                    this.message = message.message;
                } else {
                    this.message = message;
                }
                this.level = ((typeof level !== "undefined") ? level : 'error');
                this.code = ((typeof code !== "undefined") ? code : 0);
                if (!(_levels[this.level])) {
                    //an unknown error level was passed
                    throw new Error("Unknown error level: [" + this.level + "]");
                    return false;
                }
                this.throw = function () {
                    if (this.meta && this.meta.throw === true) {
                        //throw the error
                        throw new Error(this.level.toUpperCase() + ": " + this.message);
                    } else if (!this.meta) {
                        //error instance is uninitialized
                        throw new Error("Cannot throw an uninitialized error");
                    } else {
                        //use the log function specific to the level, return false
                        this.meta.debug(this.message);
                        return false;
                    }
                };
                this.log = function () {
                    //if a logging callback option is defined, logs the error
                    //if not defined, logs to the console
                };
                this.info = function () {
                    //prints information about the error to the console
                };
                this.trace = function () {
                    //prints the stacktrace to the console
                    var element, trace;
                    console.groupCollapsed("[STACKTRACE] "
                            + this.level.toUpperCase()
                            + ": " + this.message);
                    var trace = this._trace;
                    while (element = trace.shift()) {
                        console.log(element);
                    }
                    console.groupEnd();
                    return this;
                };
                if (!(this instanceof Window)) {
                    //treat instance as an error instance
                    var context = new Error();
                    this.meta = _levels[this.level];
                    this._trace = context.stack.split('\n');
                    this._trace.shift();
                    this._trace.shift();
                    return this;
                }
                return new OroborosJS.app.lib.Error(message, level, code);
            },
            Entity: function (selector, context, root) {
            },
            Event: function (trigger, context, event) {
                //"use strict";
                var OroborosEvent = function (trigger, context, event) {
                    this._trigger = trigger;
                    this._subject = context;
                    this._event = event;
                    return (this instanceof OroborosEvent ? this : new OroborosEvent(trigger, context, event));
                };
                OroborosEvent.prototype = OroborosJS.app;
                OroborosEvent.prototype.constructor = OroborosEvent;
                OroborosEvent._valid = OroborosEvent.prototype._valid = {
                    mouse: {
                        click: onclick,
                        context: oncontextmenu,
                        dblclick: ondblclick,
                        mousedown: onmousedown,
                        mouseenter: onmouseenter,
                        mouseleave: onmouseleave,
                        mousemove: onmousemove,
                        mouseover: onmouseover,
                        mouseout: onmouseout,
                        mouseup: onmouseup,
                    },
                    keyboard: {
                        keydown: onkeydown,
                        keypress: onkeypress,
                        keyup: onkeyup,
                    },
                    frame: {},
                    form: {},
                    drag: {},
                    clipboard: {},
                    print: {},
                    media: {},
                    animation: {},
                    transition: {},
                    server: {},
                    touch: {},
                };
                OroborosEvent._trigger = trigger;
                OroborosEvent._subject = oroboros(context);
                OroborosEvent._event = event;
                OroborosEvent.prototype._params = {
                    bound: false,
                    bubbles: true,
                    cancelBubble: false,
                    cancelable: true,
                    defaultPrevented: false,
                    timestamp: Date.now(),
                };
                OroborosEvent.prototype.valid = function () {
                    var valid = this._valid,
                            type = false;
                    if (!(this._subject instanceof OroborosJS.app.lib.DOMSelector)) {
                        return false;
                    }
                    for (var prop in valid) {
                        if (OroborosJS.app.utility.object.search(this._trigger, valid[prop]), 1) {
                            type = prop;
                            break;
                        }
                    }
                    return type;
                };
                OroborosEvent.prototype.bind = function () {
                    var valid = this.valid();
                    if (valid !== false) {
                        var element = this._subject.element();
                        if (!element) {
                            OroborosJS.app.error("Cannot bind to an invalid DOM element").throw();
                        }
                        element.addEventListener(this._trigger, this._event);
                        this._params.bound = true;
                        return this;
                    } else {
                        OroborosJS.app.error("Cannot bind an invalid event callback").throw();
                        return false;
                    }
                };
                OroborosEvent.prototype.unbind = function () {
                    if (!this._params.bound) {
                        return false;
                    }
                    var valid = this.valid();
                    if (valid !== false) {
                        var element = this._subject.element();
                        if (!element) {
                            OroborosJS.app.error("Cannot bind to an invalid DOM element").throw();
                        }
                        element.removeEventListener(this._trigger, this._event);
                        this._params.bound = false;
                        return this;
                    } else {
                        OroborosJS.app.error("Cannot bind an invalid event callback").throw();
                        return false;
                    }
                };
                OroborosEvent.prototype.status = function () {
                    return this._params;
                };
                OroborosEvent.prototype.param = function (key, value) {
                };
                OroborosEvent.prototype.execute = function () {
                    if (!(OroborosJS.app.utility.check(this._event) !== "function")) {
                        OroborosJS.app.error("Could not execute callback of type [" +
                                +OroborosJS.app.utility.type(this._event) +
                                +"]").throw();
                        return false;
                    }
                };
                if (this instanceof OroborosJS.app.lib.Event) {
                    return new OroborosEvent(OroborosEvent._trigger, OroborosEvent._subject, OroborosEvent._event);
                } else {
                    return new OroborosJS.app.lib.Event(trigger, context, event);
                }
            },
            DateTime: function (selector, context, root) {
            },
            DocumentOperator: function () {
                this.app = this.prototype = OroborosJS.app;
                this.location = function (key) {
                    if (typeof key === "undefined") {
                        return document.location;
                    }
                    return (typeof document.location[key] === "undefined" ? false : document.location[key]);
                };
                this.headers = function () {
                    var req = new XMLHttpRequest();
                    req.open('GET', document.location, false);
                    req.send(null);
                    var headers = {};
                    var headersRaw = OroborosJS(req.getAllResponseHeaders().toLowerCase().split('\n')).map(function (element) {
                        headers[element.split(':')[0]] = (typeof element.split(':')[1] === "string" ? element.split(':')[1].trim() : element.split(':')[1]);
                        return element;
                    });
                    return OroborosJS(headers).all();
                };
                this.title = function (value) {
                    if (typeof value === "undefined") {
                        return document.title;
                    }
                    return document.title = value;
                };
                this.height = function () {
                    return document.height;
                };
                this.width = function () {
                    return document.width;
                };
                this.ready = function (callback) {
                    if (OroborosJS.app.utility.type(callback) === "function") {
                        document.addEventListener("DOMContentLoaded", callback);
                        return true;
                    }
                    new OroborosJS.app.lib.Error("Unable to handle selector of type: ["
                            + OroborosJS.app.utility.type(callback)
                            + "]").throw();
                    return false;
                };
                return ((OroborosJS.app.utility.isWindow(this)) ? new OroborosJS.app.lib.DocumentOperator() : this);
            },
            DOMSelector: function (selector, context, root) {
                //use strict
                this.app = OroborosJS.app;
                this.element = function () {
                    return this.properties.target;
                };
                this.map = function (selector, callback) {
                    //"use strict";
                    return this;
                };
                this.empty = function () {
                    //"use strict";
                    this.html("");
                    return this;
                };
                this.append = function (payload) {
                    //"use strict";
                    if (OroborosJS.app.utility.type(payload) === "string") {
                        var old = this.properties.target.innerHTML;
                        this.properties.target.innerHTML = old + payload;
                    } else if (payload instanceof HTMLElement) {
                        console.log(payload);
                        this.properties.target.appendChild(payload);
                    } else
                        return false;
                    return this;
                };
                this.drop = function (target) {
                    //"use strict";
                    if (OroborosJS.app.utility.type(target) === "string") {
                        var remove = oroboros(target).element();
                        this.properties.target.removeChild(remove);
                    } else if (target instanceof HTMLElement) {
                        this.properties.target.removeChild(target);
                    } else
                        return false;
                    return this;
                };
                this.html = function (payload) {
                    //"use strict";
                    this.properties.target.innerHTML = payload;
                    return this;
                };
                this.tag = function (type, content) {
                    if (OroborosJS.app.utility.type(type) === "string") {
                        var newElement = document.createElement(type);
                        if (!(typeof content === "undefined")) {
                            newElement.InnerHTML = content;
                        }
                        console.dir(newElement);
                        return newElement;
                    }
                    error = OroborosJS.app.error("Tag method requires a string type parameter,\n> recieved: ["
                            + OroborosJS.app.utility.type(type) + "]", "warning");
                    if (OroborosJS.app.options("strict")) {
                        error.throw();
                    }
                    return false;
                };
                this.prepend = function (payload) {
                    //"use strict";
                    var old = this.selector.target.innerHTML;
                    this.properties.target.innerHTML = payload + old;
                    return this;
                };
                this.remove = function () {
                    //"use strict";
                };
                this.clone = function () {
                    //"use strict";
                };
                this.child = function (selector) {
                    //"use strict";
                    if (typeof selector === "undefined") {
                        //return the first direct child element
                    } else {
                        //return the specified child element
                    }
                };
                this.children = function (selector) {
                    //"use strict";
                    if (typeof selector === "undefined") {
                        //return a list of all child elements
                    } else {
                        //return a list of all child elements that match the selector
                    }
                };
                this.parent = function (selector) {
                    //"use strict";
                    if (typeof selector === "undefined") {
                        //return the direct parent element
                    } else {
                        //return the parent element that matches the specified selector
                    }
                };
                this.class = function (selector) {
                    //"use strict";
                    if (typeof selector === "undefined") {
                        //return the class of the current object
                    } else {
                        //set the class of the current object to the specified value
                    }
                };
                this.attribute = function (selector, value) {
                    //"use strict";
                };
                this.hide = function () {
                    //"use strict";
                };
                this.show = function () {
                    //"use strict";
                };
                if (typeof selector !== "undefined") {
                    var SelectorProperties;
                    this.properties = SelectorProperties = (function (selector) {
                        var res = false, result, error;
                        var SelectorProperties = {
                            selector: selector
                        };
                        switch (OroborosJS.app.utility.type(selector)) {
                            case "string":
                                try {
                                    result = document.querySelectorAll(selector);
                                    if (result.length === 1) {
                                        //single result
                                        SelectorProperties.target = SelectorProperties.selector = result[0];
                                        SelectorProperties.type = 'HTMLElement';
                                    } else if (result.length > 1) {
                                        SelectorProperties.selector = new OroborosJS.app.pattern.Iterator(result);
                                        SelectorProperties.target = SelectorProperties.selector.get();
                                        SelectorProperties.type = 'Iterator';
                                    } else {
                                        //no results
                                        return false;
                                    }
                                    res = true;
                                } catch (e) {
                                    error = OroborosJS.app.error("Invalid query selector: [" + selector + "]", "warning");
                                    if (OroborosJS.app.options("strict")) {
                                        error.throw();
                                    }
                                }
                                break;
                            case "object":
                                if (selector instanceof HTMLElement) {
                                    SelectorProperties.target = SelectorProperties.selector = selector;
                                } else if (selector instanceof NodeList) {
                                    SelectorProperties.selector = new OroborosJS.app.pattern.Iterator(result);
                                    SelectorProperties.target = SelectorProperties.selector.get();
                                    SelectorProperties.type = 'Iterator';
                                } else if (selector instanceof OroborosJS.app.lib.DOMSelector) {
                                    return selector;
                                } else if (selector instanceof OroborosJS.app.pattern.Iterator) {
                                    return selector;
                                } else {
                                    error = OroborosJS.app.error("Invalid query selector: " + selector.toString(), "warning");
                                    if (OroborosJS.app.options("strict")) {
                                        error.throw();
                                    }
                                    return false;
                                }
                                break;
                            default:
                                error = OroborosJS.app.error("Invalid query selector: [" + selector + "]").throw();
                                return false;
                                break;
                        }
                        return SelectorProperties;
                    })(selector);
                    if (!SelectorProperties) {
                        return false;
                    }
                    return (typeof this === "window" ? new OroborosJS.app.lib.DOMSelector(selector, context, root) : this);
                } else {
                    return oroboros(document);
                }
            },
            Log: function (selector, context, root) {
                var divider = "[/::]",
                        log = "",
                        logLevelDefault = "error",
                        logLevelValid = new OroborosJS.app.pattern.Iterator(["emergency", "alert", "critical", "error", "warning", "notice", "info", "debug"]);
                timestamp = function () {
                    var output = "[ " + Math.round(new Date() / 1000) + " ] ";
                    return output;
                },
                        defaultLevel = function (level) {
                            if (typeof level !== "undefined") {
                                if (logLevelValid.key(level.toLowerCase()) !== false) {
                                    logLevelDefault = level.toLowerCase();
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return logLevelDefault.toLowerCase();
                            }
                        },
                        renderLevel = function (level) {
                            if (!(OroborosJS.app.utility.type(level) === "string"
                                    || typeof level === "undefined")) {
                                error = OroborosJS.app.error("Log level parameter requires a string type,\n> recieved: ["
                                        + OroborosJS.app.utility.type(level) + "]").throw();
                                return false;
                            }
                            if (typeof level !== "undefined") {
                                if (logLevelValid.key(level.toLowerCase()) !== false) {
                                    return "[ " + level.toUpperCase() + " ] ";
                                }
                                error = OroborosJS.app.error("Invalid log level supplied", "warning");
                                if (OroborosJS.app.options("strict")) {
                                    error.throw();
                                }
                            }
                            return "[ " + defaultLevel(level).toUpperCase() + " ] ";
                        },
                        OroborosLog = function (selector, context, root) {
                            return (!(this instanceof OroborosLog) ? new OroborosLog(selector, context, root) : this);
                        };

                OroborosLog.prototype = window.oroboros.app;
                OroborosLog.prototype.constructor = OroborosLog;
                OroborosLog.prototype.render = function () {
                    console.groupCollapsed("Oroboros Log Output");
                    output = log.split(divider);
                    for (var prop in output) {
                        if (output.hasOwnProperty(prop)) {
                            console.log(output[prop]);
                        }
                    }
                    console.groupEnd();
                    return this;
                };
                OroborosLog.prototype.log = function (item, level, trace) {
                    if (typeof level === "undefined") {
                        level = "error"; //default value if not supplied
                    }
                    if (OroborosJS.app.utility.type(item) === "string") {
                        log += log + timestamp() + level(level) + item + divider;
                        return this;
                    }
                    new OroborosJS.app.lib.Error("Log entries must be in string format,\n>recieved:["
                            + OroborosJS.app.utility.type(item)
                            + "]").throw();
                    return false;
                }
                return new OroborosLog(selector, context, root);
            },
            MathElement: function (selector, context, root) {
            },
            MediaObject: function (selector, context, root) {
                //use strict
                var i, prop, resource, properties = {
                    source: undefined,
                    type: undefined,
                },
                        OroborosVideoElement = function (selector, context, root) {
                            return (this instanceof OroborosVideoElement ? this : new OroborosVideoElement(selector, context, root));
                        },
                        OroborosImageElement = function (selector, context, root) {
                            return (this instanceof OroborosImageElement ? this : new OroborosImageElement(selector, context, root));
                        },
                        OroborosAudioElement = function (selector, context, root) {
                            return (this instanceof OroborosAudioElement ? this : new OroborosAudioElement(selector, context, root));
                        },
                        OroborosEmbeddedElement = function (selector, context, root) {
                            return (this instanceof OroborosEmbeddedElement ? this : new OroborosEmbeddedElement(selector, context, root));
                        };
                OroborosVideoElement.prototype
                        = OroborosImageElement.prototype
                        = OroborosAudioElement.prototype
                        = OroborosEmbeddedElement.prototype
                        = window.oroboros.app;
                OroborosVideoElement.prototype.constructor = OroborosVideoElement;
                OroborosImageElement.prototype.constructor = OroborosImageElement;
                OroborosAudioElement.prototype.constructor = OroborosAudioElement;
                OroborosEmbeddedElement.prototype.constructor = OroborosEmbeddedElement;
                if (typeof selector !== "undefined") {

                }
                this.app = this.prototype = OroborosJS.app;
                return (typeof this === "window" ? new OroborosJS.app.lib.MediaObject() : this);
            },
            RegEx: function (selector, context, root) {
            },
            Resource: function (selector, context, root) {
            },
            Sprite: function (index, options, root) {
                //"use strict";
                var canvas = document.createElement("canvas");
                var resource = new Image();
                var i, controller, resource, prop, properties = {
                    autoInitialize: true,
                    index: index,
                    element: canvas,
                    resource: document.querySelector("#" + index),
                    scale: 100,
                    loop: false,
                    autoplay: false,
                    hidden: false,
                    animation: {
                        id: undefined,
                        spritesheet: undefined,
                        frameIndex: 0,
                        tickCount: 0,
                        ticksPerFrame: 5,
                        numberOfFrames: 1,
                        context: canvas.getContext("2d"),
                        width: 1000,
                        height: 100,
                        status: 0,
                        hidden: 0,
                        autoplay: 0
                    }
                };
                //handle arguments
                if (OroborosJS.app.utility.type(index) !== "string") {
                    new OroborosJS.app.lib.Error("[string] index parameter is required,"
                            + "\n>at oroboros.app.lib.Sprite()\n>recieved: ["
                            + OroborosJS.app.utility.type(index)
                            + "]").throw();
                    return false;
                }
                canvas.id = index;

                if (OroborosJS.app.utility.type(options) === "object") {
                    for (var prop in properties) {
                        if (properties.hasOwnProperty(prop) && !(options.hasOwnProperty(prop))) {
                            options[prop] = options[prop];
                        }
                    }
                    options.animation = ((!options.animation) ? properties.animation : options.animation);
                    for (prop in properties.animation) {
                        if (properties.animation.hasOwnProperty(prop)
                                && !(options.animation.hasOwnProperty(prop))) {
                            options.animation[prop] = properties.animation[prop];
                        }
                    }
                    options.index = index;
                }
                //declare constructor function
                var OroborosSprite = function (options) {
                    var SpriteController = {
                        context: undefined,
                        image: undefined,
                        width: undefined,
                        height: undefined
                    },
                    controller, frameIndex, tickCount,
                            ticksPerFrame, numberOfFrames;
                    SpriteController.update = function () {
                        options.animation.tickCount += 1;
                        if (options.animation.tickCount > options.animation.ticksPerFrame) {
                            options.animation.tickCount = 0;
                            // If the current frame index is in range
                            if (options.animation.frameIndex < options.animation.numberOfFrames) {
                                // Go to the next frame
                                options.animation.frameIndex += 1;
                            } else {
                                options.animation.frameIndex = 0;
                            }
                        }
                        return this;
                    };
                    SpriteController.clear = function () {
                        // Clear the canvas
                        this.context.clearRect(
                                options.animation.spritesheet.offset_x || 0,
                                options.animation.spritesheet.offset_y || 0,
                                options.animation.spritesheet.cellWidth,
                                options.animation.spritesheet.cellHeight);
                    };
                    SpriteController.render = function () {
                        var offset;
                        if (!options.animation.spritesheet) {
                            return false;
                        }
                        this.clear();
                        // Draw the animation
                        offset = this.getSpriteOffset(options.animation.frameIndex);
                        this.context.drawImage(
                                options.animation.spritesheet.resource, //ok
                                offset.x,
                                offset.y,
                                options.animation.spritesheet.cellWidth,
                                options.animation.spritesheet.cellHeight,
                                0,
                                0,
                                options.animation.spritesheet.cellWidth,
                                options.animation.spritesheet.cellHeight);
                        return this;
                    };
                    SpriteController.frame = function (index) {
                        if (index <= options.animation.numberOfFrames
                                && index >= 0) {
                            // Go to the next frame
                            options.animation.frameIndex = index;
                            return this.render();
                        }
                        return false;
                    };
                    SpriteController.next = function () {
                        if (options.animation.frameIndex < options.animation.numberOfFrames) {
                            // Go to the next frame
                            options.animation.frameIndex += 1;
                        } else {
                            frameIndex = 0;
                        }
                        return this.render();
                    };
                    SpriteController.previous = function () {
                        if (options.animation.frameIndex < options.animation.numberOfFrames) {
                            // Go to the next frame
                            options.animation.frameIndex -= 1;
                        } else {
                            options.animation.frameIndex = options.animation.numberOfFrames;
                        }
                        return this.render();
                    };
                    SpriteController.reset = function () {
                        options.animation.tickCount = 0;
                        options.animation.frameIndex = 0;
                        return this.render();
                    };
                    SpriteController.getSpriteOffset = function (frame) {
                        var VerticalOffset = 0, HorizontalOffset = 0;
                        if (options.animation.frameIndex === 0) {
                            //first frame
                            VerticalOffset = options.animation.spritesheet.offset_y;
                            HorizontalOffset = options.animation.spritesheet.offset_x;
                        } else if (options.animation.spritesheet.cells === options.animation.spritesheet.columns) {
                            //single row spritesheet
                            VerticalOffset = options.animation.spritesheet.offset_y;
                            HorizontalOffset = options.animation.spritesheet.offset_x
                                    + (options.animation.frameIndex * options.animation.spritesheet.cellWidth);
                        } else {
                            new OroborosJS.app.lib.Error("Multi-row spritesheets are not implemented.").throw();
                            return false;
                        }
                        return {
                            y: VerticalOffset,
                            x: HorizontalOffset
                        };
                    };
                    this.attach = function (element) {
                        var element = oroboros(element).element();
                        element.appendChild(canvas);
                        options.attachedTo = element;
                        controller.render();
                        return this;
                    };
                    this.options = function () {
                        return options;
                    };
                    this.detach = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        var element = options.attachedTo;
                        if (OroborosJS.app.utility.isNode(element)) {
                            element.removeChild(canvas);
                            options.attachedTo = undefined;
                            return this;
                        }
                        return false;
                    };
                    this.setOption = function (option, value) {
                    };
                    this.height = function (value) {
                        if (typeof value !== "undefined") {
                            canvas.height = value;
                            return this;
                        }
                        return canvas.height;
                    };
                    this.width = function (value) {
                        if (typeof value !== "undefined") {
                            canvas.width = value;
                            return this;
                        }
                        return canvas.width;
                    };
                    this.source = function (url) {
                        if (typeof url !== "undefined") {
                            if (OroborosJS.app.utility.type(url) === "string") {
                                resource.src = url;
                            } else {
                                return false;
                            }
                            return this;
                        }
                        return options.image;
                    };
                    this.frame = function (index) {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        result = controller.frame(index);
                        if (!result) {
                            return false;
                        }
                        return this;
                    };
                    this.next = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        controller.next();
                        return this;
                    };
                    this.previous = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        controller.previous();
                        return this;
                    };
                    this.render = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        controller.render();
                        return this;
                    };
                    this.play = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        var AnimationLoop = function () {
                            options.animation.id = window.requestAnimationFrame(AnimationLoop);
                            controller.update();
                            controller.render();
                        };
                        AnimationLoop();
                        return this;
                    };
                    this.stop = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                        if (!options.animation.id) {
                            return false;
                        }
                        window.cancelAnimationFrame(options.animation.id);
                        options.animation.id = undefined;
                        return this;
                    };
                    this.reset = function () {
                        controller.reset();
                        return this;
                    };
                    this.show = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                    };
                    this.hide = function () {
                        if (!options.initialized) {
                            this.initialize();
                        }
                    };
                    this.initialize = function () {
                        var SpriteSheet = function () {
                            this.resource = options.resource;
                            this.image = options.image;
                            this.height = undefined;
                            this.width = undefined;
                            this.cellHeight = controller.height;
                            this.cellWidth = controller.width;
                            this.offset_x = 0;
                            this.offset_y = 0;
                            this.rows = 1;
                            this.columns = 1;
                            this.cellsPerColumn = 1;
                            this.cells = options.animation.numberOfFrames;
                            this.cell = function (index) {

                            };
                            return (this instanceof SpriteSheet) ? this : new SpriteSheet();
                        };
                        SpriteSheet = SpriteSheet();
                        resource.src = options.image;
                        options.resource.addEventListener("load", function () {
//                            console.log("completing initialization...");
//                            console.log(options);
                            SpriteSheet.height = options.animation.height = this.height;
                            SpriteSheet.width = options.animation.width = this.width;
                            SpriteSheet.columns = (options.animation.numberOfFrames);
                            SpriteSheet.rows = 1;
                            options.animation.spritesheet = SpriteSheet;
                            options.initialized = true;
                        });
                        return this;
                    }
                    options.initialized = false;
                    options.element = canvas;
                    options.resource = resource;
                    canvas.height = options.height;
                    canvas.width = options.width;
                    frameIndex = options.animation.frameIndex;
                    tickCount = options.animation.tickCount;
                    ticksPerFrame = options.animation.ticksPerFrame;
                    numberOfFrames = options.animation.numberOfFrames = options.numberOfFrames;
                    SpriteController.context = options.animation.context;
                    SpriteController.width = canvas.width = options.width;
                    SpriteController.height = canvas.height = options.height;
                    SpriteController.image = options.image;
                    this.element = canvas;
                    this.controller = controller = SpriteController;
                    if (this instanceof OroborosSprite && options.autoInitialize !== false) {
                        this.initialize();
                    }
                    return ((this instanceof OroborosSprite) ? this : new OroborosSprite(options));
                };
                root = root || window.oroboros;
                OroborosSprite.prototype = root.app;
                OroborosSprite.prototype.constructor = OroborosSprite;

                //requestAnimationFrame shim
                (function () {
                    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
                    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
                    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
                    // MIT license

                    var lastTime = 0;
                    var vendors = ['ms', 'moz', 'webkit', 'o'];
                    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
                        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame']
                                || window[vendors[x] + 'CancelRequestAnimationFrame'];
                    }

                    if (!window.requestAnimationFrame)
                        window.requestAnimationFrame = function (callback, element) {
                            var currTime = new Date().getTime();
                            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                            var id = window.setTimeout(function () {
                                callback(currTime + timeToCall);
                            },
                                    timeToCall);
                            lastTime = currTime + timeToCall;
                            return id;
                        };

                    if (!window.cancelAnimationFrame)
                        window.cancelAnimationFrame = function (id) {
                            clearTimeout(id);
                        };
                }());

                return new OroborosSprite(options);
            },
            Timer: function (type, duration, callback) {
                var properties = {
                    status: 0,
                    start: 0,
                    stop: 0,
                    duration: 0,
                    loops: 0,
                    current: new Date().getTime(),
                    timeout: function (event) {
                    },
                    interval: function (event) {
                    },
                    intervalDuration: 5000,
                    pause: function (event) {
                    },
                    unpause: function (event) {
                    },
                    reset: function (event) {
                    },
                    loop: function (event) {
                    }
                },
                active = {
                    interval: null,
                    timeout: null,
                },
                        date = new Date(),
                        OroborosTimer = function (type, duration, callback) {
                            properties.duration = duration;
                            properties.timeout = callback;
                            properties.start = new Date().getTime(duration);
                            properties.end = properties.start + properties.duration;
                            return (this instanceof OroborosTimer ? this : new OroborosTimer(type, duration, callback));
                        },
                        validDurationTypes = oroboros([
                            "unix",
                            "micro",
                            "seconds"
                        ]),
                        defaultDurationType = "micro",
                        convertDuration = function (duration, resultType) {
                            if (typeof resultType == "undefined") {
                                resultType = defaultDurationType;
                            }
                            if (validDurationTypes.key(resultType) !== false) {
                                switch (resultType) {
                                    case "unix":
                                        //return a unix timestamp
                                        return Math.round(+new Date(duration) / 1000);
                                        break;
                                    case "micro":
                                        //return microtime timestamp
                                        return new Date().getTime(duration);
                                        break;
                                    case "seconds":
                                        //return seconds timestamp
                                        return new Date().getTime(duration) / 1000 | 0;
                                        break;
                                }
                            }
                            new OroborosJS.app.lib.Error("Invalid duration type, expected ["
                                    + validDurationTypes.all().join("|")
                                    + "]").throw();
                            return false;
                        };

                OroborosTimer.prototype = window.oroboros.app;
                OroborosTimer.prototype.constructor = OroborosTimer;

                OroborosTimer.prototype.start = function () {
                    properties.status = 1;
                    active.timeout = window.setTimeout(properties.timeout, properties.duration);
                    active.interval = window.setInterval(properties.interval, properties.intervalDuration);
                    properties.unpause(this);
                    return this;
                };
                OroborosTimer.prototype.stop = function () {
                    properties.status = 0;
                    window.clearTimeout(active.timeout);
                    window.clearInterval(active.interval);
                    active.timeout = active.interval = null;
                    properties.pause(this);
                    return this;
                };
                OroborosTimer.prototype.reset = function () {
                    properties.status = 0;
                    window.clearTimeout(active.timeout);
                    window.clearInterval(active.interval);
                    properties.start = new Date().getTime(duration);
                    properties.end = properties.start + properties.duration;
                    properties.reset(this);
                    return this;
                };
                OroborosTimer.prototype.clear = function () {
                    properties.status = 0;
                    window.clearTimeout(active.timeout);
                    window.clearInterval(active.interval);
                    active.timeout = active.interval = null;
                    properties.status = properties.start
                            = properties.stop = properties.duration
                            = properties.loops = 0;
                    properties.intervalDuration = 5000,
                            properties.interval = properties.timeout
                            = properties.pause = properties.unpause
                            = properties.reset = properties.loop
                            = function (event) {
                            };
                    properties.onReset();
                    return this;
                };
                OroborosTimer.prototype.loop = function () {
                    properties.loop = ((properties.loop == 1) ? 0 : 1);
                    return this;
                };
                OroborosTimer.prototype.status = function () {
                    return properties.status;
                };
                OroborosTimer.prototype.options = function (key, value) {
                    if (typeof key == "undefined") {
                        return properties;
                    } else if (typeof key !== "undefined" && typeof value == "undefined") {
                        if (properties[key]) {
                            return properties[key];
                        }
                        return false;
                    } else {
                        if (properties[key]) {
                            properties[key] = value;
                            return this;
                        }
                    }
                    return false;
                };
                OroborosTimer.prototype.interval = function (callback, duration) {
                    if (typeof callback == "undefined") {
                        return properties.interval;
                    }
                    if (typeof duration == undefined) {
                        duration = properties.intervalDuration;
                    }
                    properties.intervalDuration = Number(duration);
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.interval = callback;
                    return this;
                };
                OroborosTimer.prototype.onTimeout = function (callback) {
                    if (typeof callback == "undefined") {
                        return properties.timeout;
                    }
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.timeout = callback;
                    return this;
                };
                OroborosTimer.prototype.onPause = function (callback) {
                    if (typeof callback == "undefined") {
                        return properties.pause;
                    }
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.pause = callback;
                    return this;
                };
                OroborosTimer.prototype.onUnpause = function (callback) {
                    if (typeof callback == "undefined") {
                        return properties.unpause;
                    }
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.unpause = callback;
                    return this;
                };
                OroborosTimer.prototype.onReset = function (callback) {
                    if (typeof callback == "undefined") {
                        return properties.reset;
                    }
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.reset = callback;
                    return this;
                };
                OroborosTimer.prototype.onLoop = function (callback) {
                    if (typeof callback == "undefined") {
                        return properties.loop;
                    }
                    if (OroborosJS.app.utility.type(callback) !== "function") {
                        new OroborosJS.app.lib.Error("Invalid callback function supplied,\n> expected [function], recieved: ["
                                + OroborosJS.app.utility.type(callback)
                                + "]").throw();
                        return false;
                    }
                    properties.loop = callback;
                    return this;
                };
                if (OroborosJS.app.utility.type(duration) == "number") {
                    properties.duration = duration;
                } else if (typeof duration !== "undefined") {
                    properties.duration = Number(duration);
                }

                if (OroborosJS.app.utility.type(callback) === "function") {
                    properties.timeout = callback;
                }

                if (OroborosJS.app.utility.type(type) === "string") {

                }

                return new OroborosTimer(type, properties.duration, callback);
            },
            WindowOperator: function () {
                this.app = this.prototype = OroborosJS.app;
                return ((OroborosJS.app.utility.isWindow(this)) ? new OroborosJS.app.lib.WindowOperator() : this);
            },
            XHRObject: function (connection, context, params) {
                //use strict
                var response,
                        app = window.oroboros.app,
                        xhr = new XMLHttpRequest(),
                        properties = {
                            url: undefined,
                            headers: undefined,
                            data: undefined,
                            requestType: "get",
                            responseType: "text",
                            success: function (data, event, xhr) {
                            },
                            failure: function (data, event, xhr) {
                            },
                            sent: function (data, event, xhr) {
                            },
                            headersRecieved: function (data, event, xhr) {
                            },
                            loading: function (data, event, xhr) {
                            },
                            always: function (data, event, xhr) {
                            },
                            username: undefined,
                            password: undefined,
                            response: {
                                headers: null,
                                data: null,
                                code: 0,
                                status: 0
                            }
                        },
                processHeaders = function (headers) {
                    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    if (typeof headers == "undefined") {
                        return;
                    }
                    for (var prop in headers) {
                        if (headers.hasOwnProperty(prop)) {
                            xhr.setRequestHeader(prop, headers[prop]);
                        }
                    }
                },
                        properties_valid = oroboros("url data requestType success failure always responseType user password".split(" "));

                this.params = function (params) {
                    if ((typeof params !== "undefined")) {
                        if (OroborosJS.app.utility.type(params) === "object") {
                            for (var prop in params) {
                                if (params.hasOwnProperty(prop) && properties_valid.key(prop) !== false) {
                                    properties[prop] = params[prop];
                                }
                            }
                            return this;
                        } else {
                            new OroborosJS.app.lib.Error("Invalid XHRObject params format,\n> expected [object], recieved: ["
                                    + OroborosJS.app.utility.type(selector)
                                    + "]").throw();
                            return false;
                        }
                    } else {
                        return properties;
                    }
                };
                this.credentials = function (username, password) {
                    if (typeof username !== "undefined" || typeof password !== "undefined") {
                        if ((typeof username == "undefined" || OroborosJS.app.utility.type(username) == "string")
                                && (typeof password == "undefined" || OroborosJS.app.utility.type(password) == "string")) {
                            properties.username = username;
                            properties.password = password;
                            return this;
                        }
                        new OroborosJS.app.lib.Error("Invalid XHRObject username or password credentials,\n>expected [string] or [undefined]").throw();
                        return false;
                    } else {
                        return {
                            username: properties.username,
                            password: properties.password
                        };
                    }
                }
                this.data = function (data) {
                    if ((typeof data !== "undefined")) {
                        properties.data = data;
                        return this;
                    } else {
                        return properties.data;
                    }
                };
                this.url = function (url) {
                    if ((typeof url !== "undefined")) {
                        if (OroborosJS.app.utility.type(url) === "string") {
                            properties.url = url;
                            return this;
                        } else {
                            new OroborosJS.app.lib.Error("Invalid XHRObject connection format,\n>expected [string], recieved: ["
                                    + OroborosJS.app.utility.type(selector)
                                    + "]").throw();
                            return false;
                        }
                    } else {
                        return properties.url;
                    }
                };
                this.send = function () {
                    try {
                        xhr.open(properties.requestType, properties.url, 1, properties.username, properties.password);
                        xhr.send();
                        return this;
                    } catch (e) {
                        new OroborosJS.app.lib.Error("Could not open XHR request: " + e.message).throw();
                        return false;
                    }
                    ;
                };
                this.response = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        return properties.response;
                    } else {
                        return false;
                    }
                };
                this.request = function (type) {
                    if (typeof type !== "undefined") {
                        if (oroboros("get post put delete options head".split(" ")).key(type) !== false) {
                            properties.requestType = type;
                            return this;
                        }
                    } else {
                        return properties.responseType;
                    }
                };
                this.response = function (type) {
                    if (typeof type !== "undefined") {
                        if (oroboros("arraybuffer blob document json text moz-blob moz-chunked-text moz-chunked-arraybuffer".split(" ")).key(type) !== false) {
                            properties.responseType = type;
                            return this;
                        }
                    } else {
                        return properties.responseType;
                    }
                };
                this.status = function () {
                    return xhr.status;
                };
                this.headers = function (header, value) {
                    if (typeof header === "undefined") {
                        return ((properties.response.headers[header])
                                ? properties.response.headers[header]
                                : false);
                    } else if (typeof header !== "undefined" && typeof header !== "undefined") {
                        if (OroborosJS.app.utility.type(header) !== "string"
                                || OroborosJS.app.utility.type(header)) {
                            new OroborosJS.app.lib.Error("Invalid XHRObject.headers() parameters,\n>Expected [string]").throw();
                            return false;
                        }
                        properties.headers[header] = value;
                    } else {
                        return properties.response.headers;
                    }
                };
                this.output = function () {
                    return xhr.response;
                };
                if (typeof connection !== "undefined") {
                    this.url(connection);
                }
                this.params(params);
                this.data(context);
                xhr.onreadystatechange = function (e) {
                    var response = xhr.response;
                    properties.response.status = xhr.readyState;
                    properties.response.code = xhr.status;
                    properties.always(response, e, xhr);
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        //success
                        properties.success(response, e, xhr);
                        properties.response.data = xhr.response;
                    } else if (xhr.readyState == 4 && xhr.status !== 200) {
                        //failure
                        properties.response.data = xhr.response;
                        properties.failure(response, e, xhr);
                    } else if (xhr.readyState == 3) {
                        //Loading...
                        properties.loading(response, e, xhr);
                    } else if (xhr.readyState == 2) {
                        //Headers Recieved...
                        properties.response.headers = xhr.getAllResponseHeaders();
                        properties.headersRecieved(response, e, xhr);
                    } else if (xhr.readyState == 1) {
                        //Opened...
                        processHeaders(properties.headers);
                        properties.sent(response, e, xhr);
                    }
                }
                return (typeof this === "window" ? new OroborosJS.app.lib.XHRObject() : this);
            },
        },
        pattern: {
            Command: function (index, payload) {
            },
            Director: function (index) {
            },
            Factory: function (selector, callback, root) {
            },
            Iterator: function (selector) {
                this._pointer = 0;
                this._depth_pointer = 0;
                this._subject = null;
                this._keys = [];
                this._depth = [];
                this._mode = null;
                this.getKey = function () {
                    return this._keys[this._pointer];
                }
                /*
                 * Checks for the existence of the specified value, and returns it's key if it exists. Otherwise returns false.
                 * @param {mixed} value The value to search for.
                 * @returns {Array|Boolean} The key for the specified value, or false if it doesn't exist.
                 */
                this.key = function (value) {
                    //"use strict";
                    for (var prop in this._keys) {
                        if (this._keys.hasOwnProperty(prop) && this._subject[this._keys[prop]] === value) {
                            return this._keys[prop];
                        }
                    }
                    return false;
                };
                this.keys = function () {
                    return this._keys;
                };
                /*
                 * Returns the full iterable subset defined for this instance of the iterator. Will rebuild the correct object if required before response.
                 * @returns {object}
                 */
                this.all = function () {
                    //"use strict";
                    switch (this._mode) {
                        case 'array':
                            var result = this._subject;
                            break;
                        case 'object':
                            var result = {};
                            for (var prop in this._keys) {
                                if (this._keys.hasOwnProperty(prop)) {
                                    result[this._keys[prop]] = this._subject[prop];
                                }
                            }
                            break;
                    }
                    return result;
                };
                /*
                 * Returns the value associated with the current pointer position.
                 * @returns {mixed}
                 */
                this.get = function () {
                    //"use strict";
                    return this._subject[this._pointer];
                };
                /*
                 * Sets a new value for the current pointer position.
                 * @param {mixed} The new value for the current pointer index.
                 * @returns {void}
                 */
                this.set = function (value) {
                    //"use strict";
                    this._subject[this._pointer] = value;
                };
                /*
                 * Resets the pointer to the beginning of the iterable subset.
                 * @returns {void}
                 */
                this.rewind = function () {
                    //"use strict";
                    this._pointer = 0;
                };
                /*
                 * Sets the pointer to the specified key.
                 * @param {String} selector the key to set the pointer to.
                 * @returns {Boolean} Returns true if pointer was changed, false if key does not exist.
                 */
                this.step = function (selector) {
                    //"use strict";
                    var property = this.key(selector);
                    for (var prop in this._keys) {
                        if (this._keys[Number(prop)] == selector) {
                            this._pointer = Number(prop);
                            return true;
                        }
                    }
                    return false;
                };
                /*
                 * Moves the pointer one level forward in the iterable subset.
                 * @returns {Boolean} Returns true if a step was made, false if already at the end of the iterable subset.
                 */
                this.next = function () {
                    //"use strict";
                    var pointer = this._pointer;
                    pointer++;
                    if (pointer > this._keys.length) {
                        return false;
                    }
                    this._pointer = pointer;
                    return true;

                };
                /*
                 * Moves the pointer one level backward in the interable subset.
                 * @returns {Boolean} Returns true if a step was made, false if already at the beginning of the iterable subset.
                 */
                this.previous = function () {
                    //"use strict";
                    if (this._pointer === 0) {
                        return false;
                    }
                    this._pointer--;
                    return true;
                };
                /*
                 * Moves the pointer to the last level of the iterable subset.
                 * @returns {void}
                 */
                this.last = function () {
                    //"use strict";
                    var key = this._keys.length;
                    key--;
                    this._pointer = key;
                };
                /*
                 * Removes the last element of the iterable subset and returns it.
                 * @returns {Mixed|Boolean} The last element of the iterator subset. Returns false if the iterator is empty.
                 */
                this.pop = function () {
                    //"use strict";
                    if (this._subject.length === 0 && this._keys.length === 0) {
                        return false;
                    }
                    var result = this._subject.pop();
                    this._keys.pop();
                    if (this._pointer >= this._keys.length) {
                        var pointer = this._keys.length;
                        pointer--;
                        this._pointer = pointer;
                    }
                    return result;
                };
                /*
                 * Inserts a new element at the current pointer level, shifting existing elements forward in the process.
                 * @param {Mixed} element The new value to insert into the iterator subset
                 * @param {String|Integer} selector The key for the new value
                 * @returns {Void}
                 */
                this.poke = function (element, selector) {
                    //"use strict";
                    if (this._subject.length === 0 && this._keys.length === 0) {
                        switch (this._mode) {
                            case 'object':
                                this._subject.push(element);
                                break;
                            case 'array':
                                if (typeof (selector === 'undefined')) {
                                    throw 'Argument: selector must be defined to use OroborosIterable.prototype.poke() on an object';
                                }
                                this._keys.push(selector);
                                this._subject.push(element);
                                break;
                        }
                    } else {
                        if (this._mode === 'array' && (typeof selector !== 'undefined')) {
                            try {
                                selector.parseInt(selector);
                            } catch (e) {
                                throw 'OroborosIterable.prototype.poke(): cannot use an associative key in an array';
                            }
                        } else if (this._mode === 'array' && (typeof selector === 'undefined')) {
                            selector = newKeys.length;
                        }
                        var index = this._pointer;
                        var currKey = this._keys[index];
                        var currVal = this._subject[index];
                        var newSubj = [];
                        var newKeys = [];
                        while (newSubj.length < this._pointer) {
                            newKeys.push(this._keys.shift());
                            newSubj.push(this._subject.shift());
                        }
                        newKeys.push(selector);
                        newSubj.push(element);
                        while (this._keys.length !== 0) {
                            newKeys.push(this._keys.shift());
                            newSubj.push(this._subject.shift());
                        }
                        this._keys = newKeys;
                        this._subject = newSubj;
                    }
                };
                /*
                 * Removes the first element of the iterator subset and returns it.
                 * @returns {Mixed|Boolean} The first iterator element. Returns false if the iterator is empty.
                 */
                this.shift = function () {
                    //"use strict";
                    if (this._subject.length === 0 && this._keys.length === 0) {
                        return false;
                    }
                    var result = this._subject.shift();
                    this._keys.shift();
                    if (this._pointer !== 0) {
                        var pointer = this._pointer;
                        pointer--;
                        this._pointer = pointer;
                    }
                    return result;
                };
                /*
                 * Inserts a new first element into the iterator subset, shifting existing elements forward in the process.
                 * @param {Mixed} element The new value to insert.
                 * @param {String|Integer} selector The key for the value. If the iterator mode is array, this should not be supplied. If the iterator mode is object, this must be supplied.
                 * @returns {Void}
                 */
                this.unshift = function (element, selector) {
                    //"use strict";
                    if (this._mode === 'array' && (typeof selector !== 'undefined')) {
                        //check if the supplied selector can be cast to integer
                        //if not, throw an error, the operation cannot continue
                        try {
                            selector.parseInt(selector);
                        } catch (e) {
                            throw 'unshift() selector value must be an integer when the subject is an array';
                        }
                    } else if (this._mode === 'array' && (typeof selector === 'undefined')) {
                        //generate a key for selector 
                        //all other values must shift forward one
                        for (i = 0; i < this._keys.length; i++) {
                            var key = this._keys[i];
                            key++;
                            this._keys[i] = key;
                        }

                    }
                    this._keys.unshift(selector);
                    this._subject.unshift(element);
                    var pointer = this._pointer;
                    pointer++;
                    this._pointer = pointer;
                };
                /*
                 * Applies a function to every value within the iterable subset, returns a new Iterator object containing the results. Does not affect existing values within the current iterator.
                 * @param {function} callback The function to apply to the iterable values.
                 * @returns {OroborosIterable} Returns a new Iterator containing the affected results.
                 */
                this.map = function (callback) {
                    //"use strict";
                    if (typeof callback !== "function") {
                        throw 'each() callback must be a function.';
                    }
                    var results = [];
                    for (i = 0; i < this._keys.length; i++) {
                        results.push(callback(this._subject[this._keys[i]]));
                    }
                    return new OroborosJS.app.pattern.Iterator(results);
                };
                /*
                 * Applies a function to every value within the iterable subset. Unlike the map() function, this function will change existing values.
                 * @param {function} callback The function to apply to the iterable values.
                 * @returns {void}
                 */
                this.each = function (callback) {
                    //"use strict";
                    if (typeof callback !== "function") {
                        throw 'each() callback must be a function.';
                    }
                    for (i = 0; i < this._keys.length; i++) {
                        this._subject[this._keys[i]] = callback(this._subject[this._keys[i]], this._keys[i]);
                    }
                };
                //"use strict";
                this._pointer = 0;
                this._depth_pointer = 0;
                this._subject = selector;
                this._keys = [];
                this._depth = [];
                this._mode = OroborosJS.app.utility.type(selector);
                switch (this._mode) {
                    case 'object':
                        this._subject = [];
                        for (var prop in selector) {
                            if (selector.hasOwnProperty(prop)) {
                                this._subject.push(selector[prop]);
                                this._keys.push(prop);
                            }
                        }
                        break;
                    case 'array':
                        var count = 0;
                        for (var i in selector) {
                            if (selector.hasOwnProperty(i)) {
                                this._keys[count] = i;
                                count++;
                            }
                        }
                        break;
                }
                return ((typeof this === "window") ? new OroborosJS.app.pattern.Iterator(selector) : this);
            },
            Memento: function (parent, content) {
                this.app = this.prototype = OroborosJS.app;
                this.update = function (payload) {
                };
                this.assign = function (parent) {
                };
                this.restore = function () {
                };
                this.check = function () {
                };
                this.lock = function () {
                };
                this.unlock = function () {
                };
            },
            Observer: function () {
                this.app = this.prototype = OroborosJS.app;
            },
            Registry: function () {
                var values = {},
                        OroborosRegistry = function () {
                            this.set = function (key, value) {
                                var key = key || false,
                                        value = value || null;
                                if (key) {
                                    values[key] = value;
                                    return this;
                                }
                                return false;
                            };
                            this.get = function (key) {
                                var result = values[key];
                                return result;
                            };
                        };
            },
            State: function (states) {
                var i, prop, prop2, state,
                        properties = {},
                        defaultStateMeta = {
                            callback: function () {
                            }
                        },
                OroborosStatePattern = function (states) {
                    var defaultStateMeta = {callback: function () {
                        }}, stateset = {};
                    if (OroborosJS.app.utility.type(states) === "array") {
                        for (i = 0; i < states.length; i++) {
                            stateset[states[i]] = defaultStateMeta;
                        }
                    } else if (OroborosJS.app.utility.type(states) === "object") {
                        for (prop in states) {
                            if (states.hasOwnProperty(prop)) {
                                stateset[prop] = {callback: states[prop]};
                            }
                        }
                    } else if (typeof states == "undefined") {
                        stateset = {};
                    } else {
                        new OroborosJS.app.lib.Error("Invalid state parameter,\n>expected [array|object]").throw();
                        return false;
                    }
                    state = new OroborosJS.app.pattern.Iterator(stateset);
                    return ((this instanceof OroborosStatePattern)
                            ? this
                            : new OroborosStatePattern(states));
                };

                OroborosStatePattern.prototype = window.oroboros.app;
                OroborosStatePattern.prototype.constructor = OroborosStatePattern;
                OroborosStatePattern.prototype.current = function () {
                    return state.getKey();
                };
                OroborosStatePattern.prototype.run = function () {
                    return state.get().callback();
                };
                OroborosStatePattern.prototype.meta = function () {
                    return state.get();
                };
                OroborosStatePattern.prototype.set = function (newState) {
                    if (!(typeof newState === "undefined")) {
                        if (!state.step(newState)) {
                            new OroborosJS.app.lib.Error("Invalid state passed at OroborosState.set()").throw();
                            return false;
                        }
                        state.step(newState);
                        return state.get().callback();
                    }
                    return false;
                };
                OroborosStatePattern.prototype.stateNames = function () {
                    return state.keys();
                },
                        OroborosStatePattern.prototype.next = function () {
                            if (!state.next()) {
                                return false;
                            }
                            state.get().callback();
                            return this;
                        };
                OroborosStatePattern.prototype.previous = function () {
                    if (!state.previous()) {
                        return false;
                    }
                    state.get().callback();
                    return this;
                };
                OroborosStatePattern.prototype.reset = function () {
                    state.rewind();
                    state.get().callback();
                    return this;
                };
                OroborosStatePattern.prototype.setCallback = function (callback) {
                    if (OroborosJS.app.utility.type(callback) === "function") {
                        var newMeta = state.get();
                        newMeta.callback = callback;
                        state.set(newMeta.callback);
                        return this;
                    }
                    new OroborosJS.app.lib.Error("Invalid callback passed at OroborosState.setCallback()").throw();
                    return false;
                };
                OroborosStatePattern.prototype.updateState = function (stateName, meta) {
                    if (OroborosJS.app.utility.type(callback) === "function") {
                        var newVals = {}, prop,
                                currentKey = state.getKey(),
                                defaults = defaultStateMeta;
                        if (state.step(stateName)) {
                            for (prop in state.get()) {
                                if (meta.hasOwnProperty(prop)) {
                                    newVals[prop] = meta[prop];
                                } else {
                                    newVals[prop] = state.get()[prop];
                                }
                            }
                            state.set(newVals);
                            state.step(currentKey);
                            return this;
                        }
                        new OroborosJS.app.lib.Error("Unknown state specified at OroborosState.appendState()").throw();
                        return false;
                    }
                    new OroborosJS.app.lib.Error("Invalid parameters passed at OroborosState.appendState()").throw();
                    return false;
                };
                OroborosStatePattern.prototype.appendState = function (stateName, meta) {
                    if (OroborosJS.app.utility.type(callback) === "function") {
                        var prop,
                                currentKey = state.getKey(),
                                defaults = defaultStateMeta;
                        if (!state.step(stateName)) {
                            state.last();
                            for (prop in meta) {
                                if (meta.hasOwnProperty(prop)) {
                                    defaults[prop] = meta[prop];
                                }
                                state.poke(defaults, stateName);
                            }
                            state.step(currentKey);
                            return this;
                        } else {
                            state.step(currentKey);
                            new OroborosJS.app.lib.Error("Specified state key already exists at OroborosState.appendState()").throw();
                            return false;
                        }
                        return this;
                    }
                    new OroborosJS.app.lib.Error("Invalid parameters passed at OroborosState.appendState()").throw();
                    return false;
                };
                return new OroborosStatePattern(states);
            }
        },
        plugin: {},
        utility: {
            array: {
                like: function (obj) {
                    var length = !!obj && "length" in obj && obj.length,
                            type = OroborosJS.app.utility.type(obj);
                    if (type === "function" || OroborosJS.app.utility.isWindow(obj)) {
                        return false;
                    }
                    return type === "array" || length === 0 ||
                            typeof length === "number" && length > 0 && (length - 1) in obj;
                },
                make: function (arr, results) {
                    var ret = results || [];
                    if (arr != null) {
                        if (OroborosJS.app.utility.array.like(Object(arr))) {
                            OroborosJS.app.utility.array.merge(ret,
                                    typeof arr === "string" ?
                                    [arr] : arr
                                    );
                        } else {
                            push.call(ret, arr);
                        }
                    }
                    return ret;
                },
                merge: function (first, second) {
                    var len = +second.length,
                            j = 0,
                            i = first.length;
                    while (j < len) {
                        first[ i++ ] = second[ j++ ];
                    }
                    if (len !== len) {
                        while (second[ j ] !== undefined) {
                            first[ i++ ] = second[ j++ ];
                        }
                    }
                    first.length = i;
                    return first;
                },
            },
            object: {
                search: function (needle, haystack, searchProp) {
                    for (var prop in haystack) {
                        if ((haystack.hasOwnProperty(prop)
                                && (haystack[ prop ] == needle))
                                || (searchProp && haystack [ prop ] == needle)) {
                            return prop;
                        }
                    }
                    return false;
                },
            },
            options: {
                set: function (key, value) {
                    if ((typeof _options[key] !== "undefined")) {
                        _options[key] = value;
                        return true;
                    }
                    return false;
                },
                get: function (key) {
                    if ((typeof _options[key] !== "undefined")) {
                        return _options[key];
                    }
                    return false;
                },
                clear: function (key) {
                    if ((typeof _options[key] !== "undefined")) {
                        delete _options[key];
                        return true;
                    }
                    return false;
                },
                check: function (key, expected) {
                    return ((typeof _options[key] !== "undefined")
                            && _options[key] === expected);
                },
            },
            isWindow: function (obj) {
                return obj != null && obj == obj.window;
            },
            //Returns true if it is a DOM node
            isNode: function (o) {
                return (
                        typeof Node === "object" ? o instanceof Node :
                        o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName === "string"
                        );
            },
            //Returns true if it is a DOM element    
            isElement: function (o) {
                return (
                        typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
                        o && typeof o === "object" && o !== null && o.nodeType === 1 && typeof o.nodeName === "string"
                        );
            },
            type: function (obj) {
                if (obj == null) {
                    return obj + "";
                }
                return typeof obj === "object" || typeof obj === "function" ?
                        class2type[ toString.call(obj) ] || "object" :
                        typeof obj;
            }
        },
        // For internal use only.
        _push: push,
        _sort: deletedIds.sort,
        _splice: deletedIds.splice
    };

    /**
     * Declare Extension Method
     */
    OroborosJS.extend = OroborosJS.app.extend = function () {
        var options, name, src, copy, copyIsArray, clone,
                target = arguments[ 0 ] || {},
                i = 1,
                length = arguments.length,
                deep = false;

        // Handle a deep copy situation
        if (typeof target === "boolean") {
            deep = target;

            // Skip the boolean and the target
            target = arguments[ i ] || {};
            i++;
        }

        // Handle case when target is a string or something (possible in deep copy)
        if (typeof target !== "object" && !(OroborosJS.app.utility.type(target) === "function")) {
            target = {};
        }

        // Extend OroborosJS itself if only one argument is passed
        if (i === length) {
            target = ((typeof window.oroboros === "undefined") ? OroborosJS.app : window.oroboros.app);
            i--;
        }
        for (; i < length; i++) {
            // Only deal with non-null/undefined values
            if ((options = arguments[ i ]) != null) {

                // Extend the base object
                for (name in options) {
                    src = target[ name ];
                    copy = options[ name ];

                    // Prevent never-ending loop
                    if (target === copy) {
                        continue;
                    }

                    // Recurse if we're merging plain objects or arrays
                    if (deep && copy && (OroborosJS.isPlainObject(copy) ||
                            (copyIsArray = OroborosJS.isArray(copy)))) {

                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && OroborosJS.isArray(src) ? src : [];

                        } else {
                            clone = src && OroborosJS.isPlainObject(src) ? src : {};
                        }

                        // Never move original objects, clone them
                        target[ name ] = OroborosJS.extend(deep, clone, copy);

                        // Don't bring in undefined values
                    } else if (copy !== undefined) {
                        target[ name ] = copy;
                    }
                }
            }
        }
        // Return the modified object
        return target;
    };

    /**
     * Object Extension Operations
     */
    OroborosJS.extend({
        _plugins: function () {
            var register = new OroborosJS.app.pattern.Iterator({});
            return register;
        },
        _events: function () {
            var register = new OroborosJS.app.pattern.Iterator({});
            return register;
        },
        _resources: function () {
            var register = new OroborosJS.app.pattern.Iterator({});
            return register;
        },
        _entities: function () {
            var register = new OroborosJS.app.pattern.Iterator({});
            return register;
        },
        _permissions: function () {
            var register = new OroborosJS.app.pattern.Iterator({});
            return register;
        },
        noop: function () {
        },
    });

    // Populate the class2type map
    OroborosJS.app.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),
            function (i, name) {
                class2type[ "[object " + name + "]" ] = name.toLowerCase();
            });

    /**
     * Declare Plugin Support Method
     */
    OroborosJS.plugin = OroborosJS.app.plugin = function (selector, context) {
        var OroborosPluginIndex = {
            _plugins: OroborosJS.plugins,
            register: function (name, package) {
                var target = ((typeof window.oroboros === "undefined") ? OroborosJS.app : window.oroboros.prototype);
                var pkg = {};
                pkg[name] = this.make(((OroborosJS.app.utility.type(package) === "function") ? new package(OroborosJS) : package));
                OroborosJS.extend(target, pkg);
            },
            make: function (pkg) {
                pkg.prototype = window.oroboros.app;
                pkg.prototype.constructor = pkg;
                pkg.info = function () {
                    console.groupCollapsed(pkg.prototype.SIGNATURE + ", v: " + pkg.prototype.VERSION);
                    console.log('Copyright ' + pkg.prototype.AUTHOR);
                    console.log(pkg.prototype.LICENSE);
                    console.groupEnd();
                };
                return pkg;
            },
            deploy: function () {
            },
            info: function (package) {
                if (OroborosJS[package]) {
                    console.groupCollapsed(OroborosJS[package].SIGNATURE + ", v: " + package.VERSION);
                    console.log('Copyright ' + package.AUTHOR);
                    console.log(package.LICENSE);
                    console.groupEnd();
                } else {
                    return false;
                }

            },
            validate: function (package) {
                if (!(OroborosJS.app.utility.isWindow(package)
                        && !(OroborosJS.app.utility.type(package) === "function"))) {
                    if (OroborosJS.app.utility.type(package) === "function") {
                        //evaluate as an plugin constructor
                        return this.validate(new package(OroborosJS));
                    } else if (OroborosJS.app.utility.type(package) === "object") {
                        //evaluate as a compiled plugin object
                        if (package.SIGNATURE
                                && package.VERSION
                                && package.AUTHOR
                                && package.LICENSE) {
                            return true;
                        }
                        //console.dir(package);
                        console.info("Package does not contain plugin identity credentials.");
                    }
                }
                return false;
            }
        }
        if (!selector) {
            return OroborosPluginIndex._plugins;
        }
        switch (OroborosJS.app.utility.type(selector)) {
            case "string":
                if (OroborosPluginIndex.validate(context)) {
                    //plugin successfully validated
                    OroborosPluginIndex.register(selector, context);
                    return true;
                }
                new OroborosJS.app.lib.Error("Plugin: ["
                        + selector
                        + "] failed to validate.").throw();
                return false;
                break;
            case "function":
                //console.log(context);
                break;
            default:
                new OroborosJS.app.lib.Error("Unable to handle selector of type: ["
                        + OroborosJS.app.utility.type(selector)
                        + "]").throw();
                return false;
                break;
        }
    };

    /*
     * Declare Initialization Method
     */
    init = OroborosJS.app.init = function (selector, context, root) {
        if (typeof selector === "undefined") {
            return this;
        }
        root = root || _OroborosRoot;
        if (selector instanceof window.constructor) {
            return new OroborosJS.app.lib.WindowOperator();
        } else if (selector instanceof document.constructor) {
            return new OroborosJS.app.lib.DocumentOperator();
        }
        switch (OroborosJS.app.utility.type(selector)) {
            case "string":
                return new OroborosJS.app.lib.DOMSelector(selector);
                break;
            case "array":
                return new OroborosJS.app.pattern.Iterator(selector);
                break;
            case "object":
                if (selector instanceof HTMLDivElement) {
                    return new OroborosJS.app.lib.DOMSelector(selector, context, root);
                } else if (selector instanceof NodeList) {
                    var package = {};
                    (function (item) {
                        for (var prop in selector) {
                            if (selector.hasOwnProperty(prop)) {
                                package[prop] = oroboros(selector[prop]);
                            }
                        }
                    })(selector);
                    return new OroborosJS.app.pattern.Iterator(package);
                }
                return new OroborosJS.app.pattern.Iterator(selector);
                break;
            case "date":
                return new OroborosJS.app.lib.DateTime(selector, context, root);
                break;
            case "function":
                return new OroborosJS.app.lib.Closure(selector, context, root);
                break;
            case "number":
                return new OroborosJS.app.lib.MathElement(selector, context, root);
                break;
            case "boolean":
                return selector;
                break;
            case "regexp":
                return new OroborosJS.app.lib.RegEx(selector, context, root);
                break;
            case "symbol":
                return new OroborosJS.app.lib.Symbol(selector, context, root);
                break;
            case "error":
                return new OroborosJS.app.lib.Error(selector, context, root);
                break;
            default:
                new OroborosJS.app.lib.Error("Unable to handle selector of type: ["
                        + OroborosJS.app.utility.type(selector)
                        + "]").throw();
                return false;
                break;
        }

        if (selector.selector !== undefined) {
            this.selector = selector.selector;
            this.context = selector.context;
        } else {
            this.selector = selector;
            this.context = selector;
        }
        return OroborosJS.app.utility.array.make(selector, this);
    };
    init.prototype = OroborosJS.app;

    /**
     * Define the root object
     */
    var _OroborosRoot = OroborosJS(document);
    if (!noGlobal) {
        window.oroboros = window._o = OroborosJS;
    }
    return OroborosJS;
});